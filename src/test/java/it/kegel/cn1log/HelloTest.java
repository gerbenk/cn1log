package it.kegel.cn1log;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HelloTest {

    @Test
    public void testHello() {
        Hello hello = new Hello();
        assertEquals("Hello", hello.test());
    }
}
