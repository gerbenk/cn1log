package it.kegel.cn1log;

import io.netty.handler.codec.http.QueryStringDecoder;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServer;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ServerVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerVerticle.class);
    private Parser parser = Parser.builder().build();
    private HtmlRenderer renderer = HtmlRenderer.builder().build();
    private HttpServer server;

    @Override
    public void start(Future<Void> fut) {
        int port = config().getInteger("port", 8083);

        Router router = Router.router(vertx);
        router.get("/").handler(this::onGetRoot);
        router.get("/:id").handler(this::onGetId);
        router.post("/").handler(this::onPost);

        server = vertx
                .createHttpServer()
                .requestHandler(router::accept)
                .listen(port, ar -> fut.complete());
    }

    @Override
    public void stop() {
        server.close();
    }

    private void onGetRoot(RoutingContext context) {
        try {
            URL url = new URL("https://raw.githubusercontent.com/gerbenk/cn1log/master/readme.md");
            Node document = parser.parseReader(new BufferedReader(new InputStreamReader(url.openStream())));
            context.response().putHeader("Content-Type", "text/html").end(renderer.render(document));  // "<p>This is <em>Sparta</em></p>\n"
        } catch (IOException e) {
            context.response().setStatusCode(500).end();
        }
    }

    private void onGetId(RoutingContext context) {
        String id = context.request().getParam("id");
        String filename = "/tmp/" + id + ".log";
        LOGGER.info("Requesting " + filename);

        vertx.fileSystem().readFile(filename, ar -> {
            if (ar.succeeded()) {
                context.response().end(ar.result());
            } else {
                context.response().setStatusCode(404).end();
            }
        });
    }

    private void onPost(RoutingContext context) {
        String basePath = context.request().absoluteURI();
        System.out.println(basePath);
        String contentType = context.request().getHeader("Content-Type");

        context.request().bodyHandler(buffer -> {
            if ("application/x-www-form-urlencoded".equals(contentType)) {
                QueryStringDecoder qsd = new QueryStringDecoder(buffer.toString(), false);
                Map<String, List<String>> params = qsd.parameters();
                UUID id = store(params.toString());
                context.response().setStatusCode(200).end(basePath + id);
            } else if ("application/json".equals(contentType)) {
                UUID id = store(buffer.toString());
                context.response().setStatusCode(200).end(basePath + id);
            } else {
                System.err.println("Don't know content-type " + contentType);
                System.out.println(buffer.toString());
                context.response().setStatusCode(400).end();
            }
        });
    }

    private UUID store(String s) {
        UUID id = UUID.randomUUID();
        String filename = id + ".log";
        vertx.fileSystem().writeFile("/tmp/" + filename, Buffer.buffer(s), ar -> {
            if (ar.succeeded()) {
                LOGGER.info("Wrote file /tmp/" + filename);
            } else {
                LOGGER.error("Could not write file " + filename, ar.cause());
            }
        });
        return id;
    }
}
