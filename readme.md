# CN1Log

This is a simple tool for storing post bodies which can be used to debug mobile devices.

Usage:

Post you debug data in either "application/x-www-form-urlencoded" or "application/json" to the https://cn1log.kegel.it.
In the reply you get a link. You can review the contents behind the link.
